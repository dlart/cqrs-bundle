<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\DependencyInjection;

use Dlart\CQRS\Command\CommandBus;
use Dlart\CQRS\Query\QueryBus;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

/**
 * CQRSExtension.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class CQRSExtension extends ConfigurableExtension
{
    /**
     * @return string
     */
    public function getAlias(): string
    {
        return 'dlart_cqrs';
    }

    /**
     * @param array            $mergedConfig
     * @param ContainerBuilder $containerBuilder
     */
    protected function loadInternal(
        array $mergedConfig,
        ContainerBuilder $containerBuilder
    ): void {
        $loader = new YamlFileLoader(
            $containerBuilder,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');

        $containerBuilder->setDefinition(
            $mergedConfig['command']['bus']['service']['id'],
            new Definition(CommandBus::class)
        );

        $containerBuilder->setDefinition(
            $mergedConfig['query']['bus']['service']['id'],
            new Definition(QueryBus::class)
        );
    }
}
