<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Command;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * CommandHandlerCompilerPass.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class CommandHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        $configuration = $containerBuilder->getExtensionConfig('dlart_cqrs');

        $commandBusServiceId = $configuration['command']['bus']['service']['id'];

        if (!$containerBuilder->has($commandBusServiceId)) {
            return;
        }

        $definition = $containerBuilder->findDefinition($commandBusServiceId);

        $commandHandlerTagName = $configuration['command']['handler']['tag']['name'];

        $taggedServices = $containerBuilder->findTaggedServiceIds(
                $commandHandlerTagName
            );

        $commandAttributeNameOfCommandHandlerTag = $configuration['command']['handler']['tag']['attribute']['command']['name'];

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerHandler',
                    [
                        new Reference($id),
                        $attributes[$commandAttributeNameOfCommandHandlerTag],
                    ]
                );
            }
        }
    }
}
