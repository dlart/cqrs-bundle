<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Command;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * CommandValidatorCompilerPass.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class CommandValidatorCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        $configuration = $containerBuilder->getExtensionConfig('dlart_cqrs');

        $commandBusServiceId = $configuration['command']['bus']['service']['id'];

        if (!$containerBuilder->has($commandBusServiceId)) {
            return;
        }

        $definition = $containerBuilder->findDefinition($commandBusServiceId);

        $commandValidatorTagName = $configuration['command']['validator']['tag']['name'];

        $taggedServices = $containerBuilder->findTaggedServiceIds(
                $commandValidatorTagName
            );

        $commandAttributeNameOfCommandValidatorTag = $configuration['command']['validator']['tag']['attribute']['command']['name'];

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerValidator',
                    [
                        new Reference($id),
                        $attributes[$commandAttributeNameOfCommandValidatorTag],
                    ]
                );
            }
        }
    }
}
