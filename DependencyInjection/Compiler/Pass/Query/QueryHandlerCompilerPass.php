<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Query;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * QueryHandlerCompilerPass.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class QueryHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        $configuration = $containerBuilder->getExtensionConfig('dlart_cqrs');

        $queryBusServiceId = $configuration['query']['bus']['service']['id'];

        if (!$containerBuilder->has($queryBusServiceId)) {
            return;
        }

        $definition = $containerBuilder->findDefinition($queryBusServiceId);

        $queryHandlerTagName = $configuration['query']['handler']['tag']['name'];

        $taggedServices = $containerBuilder->findTaggedServiceIds(
                $queryHandlerTagName
            );

        $queryAttributeNameOfQueryHandlerTag = $configuration['query']['handler']['tag']['attribute']['query']['name'];

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerHandler',
                    [
                        new Reference($id),
                        $attributes[$queryAttributeNameOfQueryHandlerTag],
                    ]
                );
            }
        }
    }
}
