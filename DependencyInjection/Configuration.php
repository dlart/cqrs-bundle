<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('dlart_cqrs');

        $rootNode
            ->children()
                ->arrayNode('command')
                    ->children()
                        ->arrayNode('bus')
                            ->children()
                                ->arrayNode('service')
                                    ->children()
                                        ->scalarNode('id')
                                            ->defaultValue('dlart_cqrs.command_bus')
                                        ->end()->end()->end()->end()->end()->arrayNode(
            'handler'
        )->children()->arrayNode('tag')->children()->arrayNode(
            'attribute'
        )->children()->arrayNode('command')->children()->scalarNode(
            'name'
        )->defaultValue('command')->end()->end()->end()->end()->end(
        )->scalarNode('name')->defaultValue(
            'dlart_cqrs.command_handler'
        )->end()->end()->end()->end()->end()->arrayNode(
            'validator'
        )->children()->arrayNode('tag')->children()->arrayNode(
            'attribute'
        )->children()->arrayNode('command')->children()->scalarNode(
            'name'
        )->defaultValue('command')->end()->end()->end()->end()->end(
        )->scalarNode('name')->defaultValue(
            'dlart_cqrs.command_validator'
        )->end()->end()->end()->end()->end()->end()->end()->arrayNode(
            'query'
        )->children()->arrayNode('bus')->children()->arrayNode(
            'service'
        )->children()->scalarNode('id')->defaultValue(
            'dlart_cqrs.command_bus'
        )->end()->end()->end()->end()->end()->arrayNode(
            'handler'
        )->children()->arrayNode('tag')->children()->arrayNode(
            'attribute'
        )->children()->arrayNode('command')->children()->scalarNode(
            'name'
        )->defaultValue('query')->end()->end()->end()->end()->end()->scalarNode(
            'name'
        )->defaultValue(
            'dlart_cqrs.query_handler'
        )->end()->end()->end()->end()->end()->end()->end()->end();

        return $treeBuilder;
    }
}
