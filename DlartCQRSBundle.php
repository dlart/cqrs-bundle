<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle;

use Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Command\CommandHandlerCompilerPass;
use Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Command\CommandValidatorCompilerPass;
use Dlart\CQRSBundle\DependencyInjection\Compiler\Pass\Query\QueryHandlerCompilerPass;
use Dlart\CQRSBundle\DependencyInjection\CQRSExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * DlartCQRSBundle.
 *
 * @author Denis Lityagin <denis.lityagin@gmail.com>
 */
class DlartCQRSBundle extends Bundle
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function build(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder
            ->addCompilerPass(new CommandHandlerCompilerPass())
            ->addCompilerPass(new CommandValidatorCompilerPass())
            ->addCompilerPass(new QueryHandlerCompilerPass())
        ;
    }

    /**
     * @return CQRSExtension
     */
    public function getContainerExtension(): CQRSExtension
    {
        return new CQRSExtension();
    }
}
