# dlart/cqrs-bundle

CQRS bundle.

![Current stable version](https://img.shields.io/badge/stable-1.0.0-green.svg?style=flat)
![Minimum PHP version](https://img.shields.io/badge/php->=7.1.0-7c8cbf.svg?style=flat)
![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)

This bundle integrate [CQRS library](https://github.com/dlart/cqrs) in Symfony framework projects.

## Requirements

*  **PHP** >=7.1.0

## Installation

### Step 1: Download the Bundle

Using Composer:

```bash
composer require dlart/cqrs-bundle
```

### Step 2: Enable the Bundle

Enable the bundle by adding it to the list of registered bundles in the app/AppKernel.php file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Dlart\CQRSBundle\DlartCQRSBundle(),
        ];
        
        // ...
    }
    
    // ...
}
```

### Optional configure bundle

```yaml
dlart_cqrs:
    command:
        bus:
            service:
                id: you_own_service_id
        handler:
            tag:
                attribute:
                    command:
                        name: you_own_attribute_name
                name: yout_own_tag_name
        validator:
            tag:
                attribute:
                    command:
                        name: you_own_attribute_name
                name: yout_own_tag_name
    query:
        bus:
            service:
                id: you_own_service_id
        handler:
            tag:
                attribute:
                    command:
                        name: you_own_attribute_name
                name: yout_own_tag_name
```

## Configuring command handlers

Example:
```yaml
community.member.command.handler.register:
    arguments:
        - '@community.member.repository'
        - '@community.member.identity.generator'
        - '@core.password.encoder'
    class: Community\Member\Command\Handler\RegisterMemberCommandHandler
    tags:
        - { name: dlart_cqrs.command_handler, command: Community\Member\Command\RegisterMemberCommand }
```

## Configuring command validators

Example:
```yaml
community.member.command.validator.register:
    arguments:
        - '@dlart_cqrs.query_bus'
    class: Comunity\Member\Command\Validator\RegisterMemberCommandValidator
    tags:
        - { name: dlart_cqrs.command_validator, command: Community\Member\Command\RegisterMemberCommand }
```

## Configuring query handlers

Example:
```yaml
community.member.query.handler.is_member_account_email_already_used:
    class: Community\Member\Query\Handler\IsMemberAccountEmailAlreadyUsedQueryHandler
    lazy: true
    public: false
    tags:
        - { name: dlart_cqrs.query_handler, query: Community\Member\Query\IsMemberAccountEmailAlreadyUsedQuery }
```

Also, bundle provides an opportunity to use abstract parental services for you own query handlers:

```yaml
community.member.query.handler.is_member_account_email_already_used:
    class: Community\Member\Query\Handler\IsMemberAccountEmailAlreadyUsed\DBALIsMemberAccountEmailAlreadyUsedQueryHandler
    parent: dlart_cqrs.query.handler.dbal.abstract
    tags:
        - { name: dlart_cqrs.query_handler, query: Community\Member\Query\IsMemberAccountEmailAlreadyUsedQuery }
```

At the moment, abstract parent handlers are available:
*  DBAL: `dlart_cqrs.query.handler.dbal.abstract`

## License

Bundle is licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Copyright

2017 © Denis Lityagin ([denis.lityagin@gmail.com](mailto:denis.lityagin@gmail.com)).
