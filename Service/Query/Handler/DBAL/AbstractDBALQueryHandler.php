<?php

/*
 * This file is part of CQRS bundle package.
 *
 * (c) Denis Lityagin <denis.lityagin@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled with this
 * source code in the file LICENSE.
 */

namespace Dlart\CQRSBundle\Service\Query\Handler\DBAL;

use Dlart\CQRS\Query\Handler\AbstractQueryHandler as QueryHandler;
use Doctrine\DBAL\Connection;

/**
 * AbstractDBALQueryHandler.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractDBALQueryHandler extends QueryHandler
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
}
